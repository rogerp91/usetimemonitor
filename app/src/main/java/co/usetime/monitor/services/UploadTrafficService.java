package co.usetime.monitor.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import co.usetime.monitor.UI;
import co.usetime.monitor.domain.model.Consumptions;
import co.usetime.monitor.domain.model.TrafficInfo;
import co.usetime.monitor.domain.repository.api.HttpRestClient;
import co.usetime.monitor.domain.repository.imp.TrafficSQLRepositoryImp;
import co.usetime.monitor.domain.repository.interfaces.TrafficSQLRepository;
import co.usetime.monitor.utils.Constants;
import co.usetime.monitor.utils.Functions;
import co.usetime.monitor.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadTrafficService extends BaseService {

    protected static String TAG = UploadTrafficService.class.getSimpleName();
    private static Context context = UI.getContext;
    public static boolean INSTANCE;

    private boolean mStarted = false; // Is the Client started?
    private AlarmManager mAlarmManager;            // Alarm manager to perform repeating tasks
    private ConnectivityManager mConnectivityManager; // To check for connectivity changes
    private static final int KEEP_ALIVE = 1000 * 60 * 30; // KeepAlive Interval in MS 300000 3600000 1800000 180000 1000 * 60 * 30

    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx, UploadTrafficService.class);
        i.setAction((ManagerService.ACTION_START));
        context.startService(i);
    }

    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx, UploadTrafficService.class);
        i.setAction((ManagerService.ACTION_STOP));
        context.startService(i);
    }

    public static void actionDefinitive(Context ctx) {
        Intent i = new Intent(ctx, UploadTrafficService.class);
        i.setAction((ManagerService.ACTION_DEFINITIVE));
        context.startService(i);
    }

    public static void actionKeepalive(Context ctx) {
        Intent i = new Intent(ctx, UploadTrafficService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        context.startService(i);
    }

    public static void actionAlive(Context ctx) {
        Intent i = new Intent(ctx, UploadTrafficService.class);
        i.setAction(ManagerService.ACTION_ALIVE);
        context.startService(i);
    }

    private UploadThread mUploadThread = null;
    private HttpRestClient client = null;
    private String token;
    //TrafficRepository trafficRepository = null;
    List<Consumptions> consumptionses;
    TrafficSQLRepository trafficSQLRepository = null;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = true;
        trafficSQLRepository = new TrafficSQLRepositoryImp();
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "Starting service with no action Probably from a crash");
            stopSelf();
        } else {
            if (action.equals(ManagerService.ACTION_START)) {
                Log.i(TAG, ManagerService.ACTION_START);
                start();
            } else {
                if (action.equals(ManagerService.ACTION_STOP)) {
                    Log.i(TAG, ManagerService.ACTION_STOP);
                    stop();
                    stopSelf();
                } else {
                    if (action.equals(ManagerService.ACTION_KEEPALIVE)) {
                        keepAlive();
                    } else {
                        if (action.equals(ManagerService.ACTION_RECONNECT)) {
                            if (isNetworkAvailable()) {
                                //reconnectIfNecessary();
                            } else {
                                if (action.equals(ManagerService.ACTION_DEFINITIVE)) {
                                    definitive();
                                }
                            }
                        }
                    }
                }
            }
        }
        return Service.START_FLAG_RETRY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        INSTANCE = false;
    }

    private synchronized void start() {
        if (mStarted) {
            Log.d(TAG, "Attempt to start while already started");
            return;
        }

        if (hasScheduledKeepAlives()) {
            stopKeepAlives();
        }

        connect();
        try {
            registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (IllegalArgumentException e) {
            Log.d(TAG, e.toString());
        }
    }

    private synchronized void stop() {
        if (!mStarted) {
            Log.d(TAG, "Attemtpign to stop connection that isn't running");
            return;
        }

        mUploadThread = null;
        client = null;
        mStarted = false;
        stopKeepAlives();
        if (mConnectivityReceiver != null) {
            unregisterReceiver(mConnectivityReceiver);
            mConnectivityReceiver = null;
        }
    }

    private synchronized void definitive() {
        if (!mStarted) {
            Log.d(TAG, "Attemtpign to stop connection that isn't running");
            return;
        }

        mUploadThread = null;
        client = null;
        mStarted = false;
        stopKeepAlives2();
        if (mConnectivityReceiver != null) {
            unregisterReceiver(mConnectivityReceiver);
            mConnectivityReceiver = null;
        }
        stopSelf();
    }

    /**
     * Schedules keep alives via a PendingIntent
     * in the Alarm Manager
     */
    private void startKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, UploadTrafficService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + KEEP_ALIVE, KEEP_ALIVE, pi);
    }

    /**
     * Cancels the Pending Intent
     * in the alarm manager
     */
    private void stopKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, UploadTrafficService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.cancel(pi);
    }

    private void stopKeepAlives2() {
        Intent i = new Intent();
        i.setClass(this, UploadTrafficService.class);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.cancel(pi);
        stopService(i);
    }

    /**
     * Checkes the current connectivity
     * and reconnects if it is required.
     */
    private synchronized void reconnectIfNecessary() {
        connect();
    }

    /**
     * Query's the NetworkInfo via ConnectivityManager
     * to return the current connected state
     *
     * @return boolean true if we are connected false otherwise
     */
    private boolean isNetworkAvailable() {
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }


    /**
     * Receiver that listens for connectivity chanes
     * via ConnectivityManager
     */
    private BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.d(TAG, "Connectivity Changed...");
        }
    };

    /**
     * Query's the AlarmManager to check if there is
     * a keep alive currently scheduled
     *
     * @return true if there is currently one scheduled false otherwise
     */
    private synchronized boolean hasScheduledKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, UploadTrafficService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, PendingIntent.FLAG_NO_CREATE);
        return (pi != null);
    }

    private synchronized void connect() {
        //Log.d(TAG, "Connecting...");
        consumptionses = new ArrayList<>();
        List<TrafficInfo> traffics;
        token = Prefs.getString(Constants.TOKEN, "");
        if (trafficSQLRepository == null) {
            trafficSQLRepository = new TrafficSQLRepositoryImp();
        }
        if (client == null) {
            client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient2(120, 120, 120)));
        }
        traffics = trafficSQLRepository.query();
        if (traffics.size() > 0) {
            //Log.d(TAG, "traffics.size() > 0");
            consumptionses = new ArrayList<>();
            for (TrafficInfo traffic : traffics) {
                Consumptions consumptions = new Consumptions();
                consumptions.setName(traffic.getAppName());
                consumptions.setDate(traffic.getDate());
                consumptions.set_package(traffic.getPackageName());
                consumptions.setMobile_kb(traffic.getGPRS());
                consumptions.setWifi_kb(traffic.getWIFI());
                consumptionses.add(consumptions);
                //Log.d(TAG, traffic.getPackageName() + " - WIFI - " + Long.toString(traffic.getWIFI())+" - Mobilr - " + Long.toString(traffic.getGPRS()));
            }
        } else {
            startKeepAlives();
        }
        if (Functions.isOnline()) {
            mUploadThread = new UploadThread();
            mUploadThread.start();
        } else {
            startKeepAlives();
        }
    }

    /**
     * Publishes a KeepALive to the topic
     * in the broker
     */
    private synchronized void keepAlive() {
        Log.d(TAG, "keepAlive");
        reconnectIfNecessary();
    }

    private class UploadThread extends Thread {

        @Override
        public void run() {
            super.run();
            Log.d(TAG, "Connecting with URL: " + HttpRestClient.BASE_URL + HttpRestClient.VERSION + "data_consumptions");
            String arrayListToJson = new Gson().toJson(consumptionses);
            //System.out.println(TAG + " -- " + arrayListToJson);
            if (!arrayListToJson.equals("[]")) {
                Call<Void> voidCall = client.performConsumptions(HttpRestClient.PREFIX + token, consumptionses);
                voidCall.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        switch (response.code()) {
                            case 200:
                                System.out.println(response.body());
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                //trafficRepository.clear();
                                startKeepAlives();
                                break;
                            case 204:
                                //trafficRepository.clear();
                                //startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                break;
                            case 400:
                                //startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                System.out.println(response.body());
                                break;
                            case 401:
                                //startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                break;
                            case 403:
                                //startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                break;
                            case 404:
                                //startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                break;
                            case 422:
                                startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                break;
                            case 500:
                                startKeepAlives();
                                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Log.e(TAG, t.toString());
                        startKeepAlives();
                    }
                });
            } else {
                startKeepAlives();
            }
        }
    }//endClass
}//endClass