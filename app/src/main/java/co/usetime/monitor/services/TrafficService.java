package co.usetime.monitor.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.usetime.monitor.UI;
import co.usetime.monitor.domain.model.TrafficInfo;
import co.usetime.monitor.domain.repository.imp.TrafficSQLRepositoryImp;
import co.usetime.monitor.domain.repository.interfaces.TrafficSQLRepository;
import co.usetime.monitor.utils.Prefs;


/**
 * Created by Roger Patiño on 06/04/2016.
 */
public class TrafficService extends BaseService {
    protected static String TAG = TrafficService.class.getSimpleName();
    private static Context context = UI.getContext;

    public static void actionStart() {
        Intent i = new Intent(context, TrafficService.class);
        i.setAction((ManagerService.ACTION_START));
        context.startService(i);
    }

    public static void actionStop() {
        Intent i = new Intent(context, TrafficService.class);
        i.setAction((ManagerService.ACTION_STOP));
        context.stopService(i);
    }

    public static void actionDefinitive(Context ctx) {
        Intent i = new Intent(ctx, UploadTrafficService.class);
        i.setAction((ManagerService.ACTION_DEFINITIVE));
        context.startService(i);
    }

    private TrafficReceiver tReceiver;
    private WifiManager wifiManager;
    private ConnectivityManager cManager;
    private PackageManager pm;

    List<TrafficInfo> trafficInfosOrigin = new ArrayList<>();
    List<TrafficInfo> trafficInfosWifi;
    List<TrafficInfo> trafficInfosGprs;

    private boolean isWIFI;
    private boolean isGPRS;

    TrafficSQLRepository trafficSQLRepository = new TrafficSQLRepositoryImp();

    IntentFilter filter = null;

    @Override
    public void onCreate() {
        super.onCreate();
        wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        cManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        Log.w(TAG, "Service Start");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "Starting service with no action Probably fro m a crash");
            stopSelf();
        } else {
            if (action.equals(ManagerService.ACTION_START)) {
                Log.w(TAG, ManagerService.ACTION_START);
                start();
            } else {
                if (action.equals(ManagerService.ACTION_STOP)) {
                    Log.w(TAG, ManagerService.ACTION_STOP);
                    stop();
                }
            }
        }
        return Service.START_FLAG_REDELIVERY;
    }

    /**
     * comenzar el servicio
     */
    private synchronized void start() {
        tReceiver = new TrafficReceiver();
        if (filter == null) {
            filter = new IntentFilter();
            filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(tReceiver, filter);
        }

        if (trafficSQLRepository == null) {
            trafficSQLRepository = new TrafficSQLRepositoryImp();
        }

    }

    /**
     * detener el servicio
     */
    private synchronized void stop() {
        trafficSQLRepository.close();
        trafficSQLRepository = null;
        if (tReceiver != null) {
            unregisterReceiver(tReceiver);
            tReceiver = null;
        }
        stopSelf();
    }

    private class TrafficReceiver extends BroadcastReceiver {
        private String action = "";
        private String BROADCAST = TrafficReceiver.class.getSimpleName();

        @Override
        public void onReceive(Context context, Intent intent) {
            action = intent.getAction();
            if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                isWIFI = true;
                if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                    Log.w(BROADCAST, "WIFI_STATE_ENABLED");
                    Log.w(BROADCAST, "WIFI_STATE_ENABLED");
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            while (isWIFI) {
                                try {
                                    Thread.sleep(2000);
                                    Log.w(BROADCAST, "WIFI>>>>>start");
                                    trafficInfosWifi = new ArrayList<>();
                                    trafficInfosWifi = getTrafficInfos();
                                    for (TrafficInfo infoO : trafficInfosOrigin)
                                        for (TrafficInfo infoW : trafficInfosWifi) {
                                            Log.w(BROADCAST, "TrafficInfo infoW : trafficInfosWifi");
                                            long traffic = infoW.getTraffic() - infoO.getTraffic();
                                            if (infoO.getPackageName().equals(infoW.getPackageName()) && traffic != 0) {
                                                Log.w(BROADCAST, "infoO.getPackageName().equals(infoW.getPackageName()) && traffic != 0");
                                                trafficSQLRepository.open();
                                                trafficSQLRepository.insertTrafficWIFI(traffic, infoW.getPackageName());
                                                Log.w(TAG, infoW.getPackageName() + "WIFI:" + Formatter.formatFileSize(TrafficService.this, traffic));
                                            }
                                        }
                                    trafficInfosOrigin = trafficInfosWifi;
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).start();
                } else if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED) {
                    Log.w(BROADCAST, "WIFI_STATE_DISABLED");
                    isWIFI = false;
                    Log.w(BROADCAST, "WIFI>>>>>end");
                }
            } else if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                Log.w(BROADCAST, "CONNECTIVITY_ACTION");
                NetworkInfo networkInfo = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                NetworkInfo.State state = networkInfo.getState();
                if (state == NetworkInfo.State.CONNECTED && !(wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED)) {
                    Log.w(BROADCAST, "State.CONNECTED");
                    isGPRS = true;
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            while (isGPRS) {
                                try {
                                    Thread.sleep(2000);
                                    Log.w(BROADCAST, "GPRS>>>>>start");
                                    trafficInfosGprs = new ArrayList<>();
                                    trafficInfosGprs = getTrafficInfos();
                                    for (TrafficInfo infoO : trafficInfosOrigin)
                                        for (TrafficInfo infoG : trafficInfosGprs) {
                                            long traffic = infoG.getTraffic() - infoO.getTraffic();
                                            if (infoO.getPackageName().equals(infoG.getPackageName()) && traffic != 0) {
                                                trafficSQLRepository.open();
                                                trafficSQLRepository.insertTrafficGPRS(traffic, infoG.getPackageName());
                                                Log.w(TAG, infoG.getPackageName() + "GPRS:" + Formatter.formatFileSize(TrafficService.this, traffic));
                                            }
                                        }
                                    trafficInfosOrigin = trafficInfosGprs;
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }).start();
                } else if (state == NetworkInfo.State.DISCONNECTED) {
                    Log.w(BROADCAST, "State.DISCONNECTED");
                    isGPRS = false;
                    Log.w(BROADCAST, "¯GPRS>>>>>end");
                }
            } else {
                Prefs.clear();
                try {
                    TrafficSQLRepository trafficSQLRepository = new TrafficSQLRepositoryImp();
                    trafficSQLRepository.clear();
                } catch (SQLiteException e) {
                    Log.d(TAG, e.toString());
                }
                stopSelf();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
    }

    /**
     * @return listado de trafico
     */
    private List<TrafficInfo> getTrafficInfos() {
        Log.w(TAG, "getTrafficInfos");
        List<TrafficInfo> trafficInfos = new ArrayList<>();
        pm = getPackageManager();
        List<PackageInfo> packinfos = pm.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES | PackageManager.GET_PERMISSIONS);
        for (PackageInfo info : packinfos) {
            String[] premissions = info.requestedPermissions;
            if (premissions != null && premissions.length > 0) {
                for (String premission : premissions) {
                    if ("android.permission.INTERNET".equals(premission)) {
                        int uid = info.applicationInfo.uid;
                        long total = TrafficStats.getUidRxBytes(uid) + TrafficStats.getUidTxBytes(uid);
                        if (total < 0) {
                            TrafficInfo trafficInfo = new TrafficInfo();
                            trafficInfo.setPackageName(info.packageName);
                            trafficInfo.setTraffic(0);
                            trafficInfos.add(trafficInfo);
                        } else {
                            TrafficInfo trafficInfo = new TrafficInfo();
                            trafficInfo.setPackageName(info.packageName);
                            trafficInfo.setTraffic(total);
                            trafficInfos.add(trafficInfo);
                        }
                    }
                }
            }
        }
        return trafficInfos;
    }
}