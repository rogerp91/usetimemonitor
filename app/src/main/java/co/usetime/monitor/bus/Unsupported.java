package co.usetime.monitor.bus;

/**
 * Created by Roger Patiño on 20/05/2016.
 */
public class Unsupported {

    boolean aBoolean;

    public Unsupported(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public boolean isaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }
}