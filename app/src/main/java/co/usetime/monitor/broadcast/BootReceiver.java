package co.usetime.monitor.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.usetime.monitor.services.TrafficService;
import co.usetime.monitor.services.UploadTrafficService;

/**
 * Created by Roger Patiño on 08/04/2016.
 */
public class BootReceiver extends BroadcastReceiver {
    protected static String TAG = BootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
                Log.d(TAG, "ACTION_BOOT_COMPLETED");
                TrafficService.actionStart();
                UploadTrafficService.actionStart(context);
            }
        } else {
            Log.d(TAG, "BroadcastReceiver == null");
        }
    }
}