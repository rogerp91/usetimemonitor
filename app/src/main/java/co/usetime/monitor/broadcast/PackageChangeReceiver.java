package co.usetime.monitor.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Roger Patiño on 08/04/2016.
 */
public class PackageChangeReceiver extends BroadcastReceiver {
    protected static String TAG = PackageChangeReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "PackageChangeReceiver...");
        if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
            Log.d(TAG, "ACTION_POWER_CONNECTED");
        } else {
            if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)) {
                Log.d(TAG, "ACTION_POWER_DISCONNECTED");
            } else {
                if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
                    Log.d(TAG, "ACTION_PACKAGE_ADDED");
                } else {
                    if (intent.getAction().equals(Intent.ACTION_PACKAGE_CHANGED)) {
                        Log.d(TAG, "ACTION_PACKAGE_CHANGED");
                    } else {
                        if (intent.getAction().equals(Intent.ACTION_PACKAGE_RESTARTED)) {
                            Log.d(TAG, "ACTION_PACKAGE_RESTARTED");
                        } else {
                            if (intent.getAction().equals(Intent.ACTION_PACKAGE_DATA_CLEARED)) {
                                Log.d(TAG, "ACTION_PACKAGE_DATA_CLEARED");
                            }else{
                                if(intent.getAction().equals(Intent.ACTION_PACKAGE_FULLY_REMOVED)){
                                    Log.d(TAG, "ACTION_PACKAGE_DATA_CLEARED");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
