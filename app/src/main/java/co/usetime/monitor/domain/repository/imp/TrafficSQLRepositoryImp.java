package co.usetime.monitor.domain.repository.imp;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import co.usetime.monitor.UI;
import co.usetime.monitor.domain.model.TrafficInfo;
import co.usetime.monitor.domain.repository.interfaces.TrafficSQLRepository;

/**
 * Created by Roger Patiño on 25/05/2016.
 */
public class TrafficSQLRepositoryImp implements TrafficSQLRepository {
    protected static String TAG = TrafficSQLRepositoryImp.class.getSimpleName();

    private SQLiteDatabase database;
    private TrafficSQL dbHelper;

    public TrafficSQLRepositoryImp() {
        dbHelper = new TrafficSQL();
    }

    @Override
    public void open() {
        Log.w(TAG, "Se abre conexion a la base de datos " + dbHelper.getDatabaseName());
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        Log.w(TAG, "Se cierra conexion a la base de datos " + dbHelper.getDatabaseName());
        dbHelper.close();
    }

    @Override
    public void insertTrafficGPRS(long traffic, String packagename) {
        //Log.w(TAG, "insertTrafficGPRS");
        database = dbHelper.getWritableDatabase();
        String sql = "insert into Traffic(GPRS, packagename) values(" + traffic + ",'" + packagename + "')";
        Log.i(TAG, sql);
        database.execSQL(sql);
        //Log.w(TAG, "insertTrafficGPRS>>>>>>>>>>>");
        //database.close();
    }

    @Override
    public void insertTrafficWIFI(long traffic, String packagename) {
        //Log.w(TAG, "insertTrafficWIFI");
        database = dbHelper.getWritableDatabase();
        String sql = "insert into Traffic(WIFI, packagename) values(" + traffic + ",'" + packagename + "')";
        Log.i(TAG, sql);
        database.execSQL(sql);
        //Log.w("TrafficDb", "insertTrafficWIFI>>>>>>>>>>>");
        //database.close();
    }

    @Override
    public List<TrafficInfo> queryByTime(String time) {
        database = dbHelper.getWritableDatabase();
        String time1;
        String time2;
        Date d = new Date();
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        if (time.equals("today")) {
            time1 = sDateFormat.format(d.getTime());
            time2 = sDateFormat.format(new Date(d.getTime() + 24 * 60 * 60 * 1000));
        } else if (time.equals("yesterday")) {
            time1 = sDateFormat.format(new Date(d.getTime() - 24 * 60 * 60 * 1000));
            time2 = sDateFormat.format(d.getTime());
        } else {
            Calendar a = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            a.set(Calendar.DATE, 1);//
            time1 = df.format(a.getTime());
            a.roll(Calendar.DATE, -1);//
            time2 = df.format(a.getTime());
        }

        String sql = "SELECT SUM(WIFI), SUM(GPRS), packagename FROM Traffic WHERE time>='" + time1 + "' AND time<'" + time2 + "' GROUD BY packagename";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            long WIFILong = cursor.getLong(0);
            long GPRSLong = cursor.getLong(1);
            String PackNameString = cursor.getString(2);
            //Log.w(TAG, GPRSLong + "," + WIFILong + PackNameString);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(PackNameString);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(PackNameString, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            temp.setGPRS(GPRSLong);
            temp.setWIFI(WIFILong);
            appList.add(temp);
        }

        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public List<TrafficInfo> query() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT WIFI,GPRS,time,packagename FROM Traffic";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            //int id = cursor.getInt(cursor.getColumnIndex("id"));
            long WIFILong = cursor.getLong(cursor.getColumnIndex("WIFI"));
            long GPRSLong = cursor.getLong(cursor.getColumnIndex("GPRS"));
            String time = cursor.getString(cursor.getColumnIndex("time"));
            String packagename = cursor.getString(cursor.getColumnIndex("packagename"));
            // Log.w(TAG, GPRSLong + "," + WIFILong + packagename);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(packagename);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(packagename, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            temp.setPackageName(packagename);
            temp.setGPRS(GPRSLong);
            temp.setWIFI(WIFILong);
            temp.setDate(time);
            appList.add(temp);
        }
        cursor.close();
        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public List<TrafficInfo> queryWIFI() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT id, WIFI, time, packagename FROM Traffic GROUD BY WIFI";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            long WIFILong = cursor.getLong(cursor.getColumnIndex("WIFI"));
            String time = cursor.getString(cursor.getColumnIndex("time"));
            String packagename = cursor.getString(cursor.getColumnIndex("packagename"));
            //Log.w(TAG, WIFILong + packagename);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(packagename);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(packagename, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            temp.setPackageName(packagename);
            temp.setWIFI(WIFILong);
            appList.add(temp);
        }
        cursor.close();
        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        cursor.close();
        database.close();
        return appList;
    }

    @Override
    public Long queryWIFINow() {
        database = dbHelper.getWritableDatabase();
        String time1;
        String time2;
        Date d = new Date();
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        time1 = sDateFormat.format(d.getTime());
        time2 = sDateFormat.format(new Date(d.getTime() + 24 * 60 * 60 * 1000));

        String sql = "SELECT SUM(WIFI) FROM Traffic WHERE time>='" + time1 + "' AND time<'" + time2 + "'";
        Log.i(TAG, sql);
        long WIFILong = 0;
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            WIFILong = cursor.getLong(0);
        }
        cursor.close();
        database.close();
        return WIFILong;
    }

    @Override
    public Long queryGPRSNow() {
        database = dbHelper.getWritableDatabase();
        String time1;
        String time2;
        Date d = new Date();
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        time1 = sDateFormat.format(d.getTime());
        time2 = sDateFormat.format(new Date(d.getTime() + 24 * 60 * 60 * 1000));

        String sql = "SELECT SUM(GPRS) FROM Traffic WHERE time>='" + time1 + "' AND time<'" + time2 + "'";
        Log.i(TAG, sql);
        long GPRSLong = 0;
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            GPRSLong = cursor.getLong(0);
        }
        cursor.close();
        database.close();
        return GPRSLong;
    }

    @Override
    public List<TrafficInfo> queryByTimeWIFI(String time) {
        database = dbHelper.getWritableDatabase();
        String time1;
        String time2;
        Date d = new Date();
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        if (time.equals("today")) {
            time1 = sDateFormat.format(d.getTime());
            time2 = sDateFormat.format(new Date(d.getTime() + 24 * 60 * 60 * 1000));
        } else if (time.equals("yesterday")) {
            time1 = sDateFormat.format(new Date(d.getTime() - 24 * 60 * 60 * 1000));
            time2 = sDateFormat.format(d.getTime());
        } else {
            Calendar a = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            a.set(Calendar.DATE, 1);//
            time1 = df.format(a.getTime());
            a.roll(Calendar.DATE, -1);//
            time2 = df.format(a.getTime());
        }

        String sql = "SELECT SUM(WIFI), packagename FROM Traffic WHWRE time>='" + time1 + "' AND time<'" + time2 + "' GROUD BY packagename";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            long WIFILong = cursor.getLong(0);
            long GPRSLong = cursor.getLong(1);
            String PackNameString = cursor.getString(2);
            //Log.w(TAG, GPRSLong + "," + WIFILong + PackNameString);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(PackNameString);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(PackNameString, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            temp.setGPRS(GPRSLong);
            temp.setWIFI(WIFILong);
            appList.add(temp);
        }

        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public List<TrafficInfo> queryWIFI(String packagename) {
        return null;
    }

    @Override
    public List<TrafficInfo> queryGPRS() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT id, GPRS, time, packagename FROM Traffic";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            long GPRSLong = cursor.getLong(cursor.getColumnIndex("GPRS"));
            String time = cursor.getString(cursor.getColumnIndex("time"));
            String packagename = cursor.getString(cursor.getColumnIndex("packagename"));
            //Log.w(TAG, GPRSLong + "," + packagename);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(packagename);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(packagename, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            temp.setPackageName(packagename);
            temp.setGPRS(GPRSLong);
            appList.add(temp);
        }
        cursor.close();
        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public List<TrafficInfo> queryGPRS(String packagename2) {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT  id, GPRS, time, packagename FROM Traffic WHERE packagename='" + packagename2 + "'";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            long GPRSLong = cursor.getLong(cursor.getColumnIndex("GPRS"));
            String time = cursor.getString(cursor.getColumnIndex("time"));
            String packagename = cursor.getString(cursor.getColumnIndex("packagename"));
            //Log.w(TAG, GPRSLong + "," + packagename);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(packagename);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(packagename, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            temp.setPackageName(packagename);
            temp.setGPRS(GPRSLong);
            appList.add(temp);
        }
        cursor.close();
        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public List<TrafficInfo> queryByTimeGPRS(String time) {
        database = dbHelper.getWritableDatabase();
        String time1;
        String time2;
        Date d = new Date();
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        if (time.equals("today")) {
            time1 = sDateFormat.format(d.getTime());
            time2 = sDateFormat.format(new Date(d.getTime() + 24 * 60 * 60 * 1000));
        } else if (time.equals("yesterday")) {
            time1 = sDateFormat.format(new Date(d.getTime() - 24 * 60 * 60 * 1000));
            time2 = sDateFormat.format(d.getTime());
        } else {
            Calendar a = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            a.set(Calendar.DATE, 1);//
            time1 = df.format(a.getTime());
            a.roll(Calendar.DATE, -1);//
            time2 = df.format(a.getTime());
        }

        String sql = "SELECT SUM(GPRS), packagename FROM Traffic WHERE time>='" + time1 + "' AND time<'" + time2 + "' GROUD BY packagename";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            long WIFILong = cursor.getLong(0);
            long GPRSLong = cursor.getLong(1);
            String PackNameString = cursor.getString(2);
            //Log.w(TAG, GPRSLong + "," + WIFILong + PackNameString);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(PackNameString);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(PackNameString, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            temp.setGPRS(GPRSLong);
            temp.setWIFI(WIFILong);
            appList.add(temp);
        }

        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public Long querySumWifi() {
        long WIFILong = 0;
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT SUM(WIFI) FROM Traffic";
        Log.i(TAG, sql);
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            WIFILong = cursor.getLong(0);
        }
        database.close();
        return WIFILong;
    }

    @Override
    public Long querySumGPRS() {
        long GPRSLong = 0;
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT SUM(GPRS) FROM Traffic";
        Log.i(TAG, sql);
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            GPRSLong = cursor.getLong(0);
        }
        //database.close();
        return GPRSLong;
    }

    @Override
    public List<TrafficInfo> getTotalPackageGPRS() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT SUM(GPRS), packagename FROM Traffic GROUP BY packagename";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            long GPRSLong = cursor.getLong(0);
            String PackNameString = cursor.getString(1);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(PackNameString);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(PackNameString, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            temp.setGPRS(GPRSLong);
            appList.add(temp);
        }

        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public List<TrafficInfo> getTotalPackageWIFI() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT SUM(WIFI), packagename FROM Traffic GROUP BY packagename";
        Log.i(TAG, sql);
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            long WIFILong = cursor.getLong(0);
            String PackNameString = cursor.getString(1);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(PackNameString);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(PackNameString, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            temp.setWIFI(WIFILong);
            appList.add(temp);
        }

        Collections.sort(appList, new Comparator<TrafficInfo>() {
            @Override
            public int compare(TrafficInfo info0, TrafficInfo info1) {
                if (info0.getGPRS() != info1.getGPRS()) {
                    return (int) (info1.getGPRS() - info0.getGPRS());
                } else {
                    return (int) (info1.getWIFI() - info0.getWIFI());
                }
            }
        });
        //database.close();
        return appList;
    }

    @Override
    public int getCount() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT COUNT(*) FROM Traffic";
        Log.i(TAG, sql);
        Cursor cursor = database.rawQuery(sql, null);
        return cursor.getCount();
    }

    @Override
    public List<TrafficInfo> getTime() {
        String sql = "SELECT time, packagename FROM Traffic GROUD BY packagename";
        Log.i(TAG, sql);
        return null;
    }

    @Override
    public Long getTotalGPRS() {
        return null;
    }

    @Override
    public Long getTotalWIFI() {
        return null;
    }

    @Override
    public List<TrafficInfo> query2() {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT (SELECT time FROM Traffic t1 WHERE t1.packagename=t2.packagename ORDER BY time DESC LIMIT 1) AS time , SUM(WIFI) AS wifi, SUM(GPRS) AS mobile, packagename AS pakage FROM Traffic t2 GROUP BY packagename";
        Log.i(TAG, sql);
        //Log.i(TAG, Integer.toString(cursor.getCount()));
        List<TrafficInfo> appList = new ArrayList<>();
        PackageManager pm = UI.getContext.getPackageManager();
        Cursor cursor = database.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            String time = cursor.getString(0);
            long WIFILong = cursor.getLong(1);
            long GPRSLong = cursor.getLong(2);
            String PackNameString = cursor.getString(3);
            //Log.w(TAG, GPRSLong + "," + WIFILong + PackNameString);
            TrafficInfo temp = new TrafficInfo();
            try {
                Drawable icon = pm.getApplicationIcon(PackNameString);
                String name = pm.getApplicationLabel(pm.getApplicationInfo(PackNameString, 0)).toString();
                temp.setAppicon(icon);
                temp.setAppName(name);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            temp.setPackageName(PackNameString);
            temp.setGPRS(GPRSLong);
            temp.setWIFI(WIFILong);
            temp.setDate(time);
            appList.add(temp);
        }
        return appList;
    }

    @Override
    public void query3(String dates) {
        database = dbHelper.getWritableDatabase();
        String sql = "SELECT (SELECT time FROM Traffic t1 WHERE t1.packagename=t2.packagename ORDER BY time DESC LIMIT 1) AS time , SUM(WIFI) AS wifi, SUM(GPRS) AS mobile FROM Traffic t2 packagename";

    }

    @Override
    public void clear() {
        Log.w(TAG, "clear");
        dbHelper = new TrafficSQL();
        database = dbHelper.getWritableDatabase();
        dbHelper.onDelete(database);
    }
}