package co.usetime.monitor.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 20/05/2016.
 */
public class Consumptions {

    @SerializedName("name")
    private String name;

    @SerializedName("date")
    private String date;

    @SerializedName("time")
    private String time;

    @SerializedName("duration")
    private String duration;

    @SerializedName("_package")
    private String _package;

    @SerializedName("wifi_kb")
    private double wifi_kb;

    @SerializedName("mobile_kb")
    private double mobile_kb;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String get_package() {
        return _package;
    }

    public void set_package(String _package) {
        this._package = _package;
    }

    public double getWifi_kb() {
        return wifi_kb;
    }

    public void setWifi_kb(double wifi_kb) {
        this.wifi_kb = wifi_kb;
    }

    public double getMobile_kb() {
        return mobile_kb;
    }

    public void setMobile_kb(double mobile_kb) {
        this.mobile_kb = mobile_kb;
    }
}