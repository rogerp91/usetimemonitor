package co.usetime.monitor.domain.repository.api;

import java.util.List;

import co.usetime.monitor.BuildConfig;
import co.usetime.monitor.domain.model.Consumptions;
import co.usetime.monitor.domain.model.Login;
import co.usetime.monitor.domain.model.Profile;
import co.usetime.monitor.domain.model.Token;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public interface HttpRestClient {

    public static String BASE_URL = BuildConfig.HOST;
    public static String BASE_URL_2 = "https://beta.usetime.co/api";
    public static String VERSION = "v1/";
    public static String PREFIX = "Bearer ";
    public static String USER_AGENT = "Usetime-Android/";


    @Headers("Content-Type: application/json")
    @POST(VERSION + "auth/login")
    Call<Token> performLogin(@Header("User-Agent") String agent, @Body Login login);

    /**
     * @param authorization token
     * @return
     */
    //perfil
    @Headers("Accept: application/json")
    @GET(VERSION + "profile/me")
    Call<Profile> performProfile(@Header("Authorization") String authorization);

    @Headers("Accept: application/json")
    @POST(VERSION + "data_consumptions")
    Call<Void> performConsumptions(@Header("Authorization") String authorization, @Body List<Consumptions> consumptionsList);


}