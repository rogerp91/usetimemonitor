package co.usetime.monitor.domain.repository.imp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import co.usetime.monitor.UI;
import co.usetime.monitor.utils.Constants;

/**
 * Created by Roger Patiño on 25/05/2016.
 */
public class TrafficSQL extends SQLiteOpenHelper {

    protected static String TAG = TrafficSQL.class.getSimpleName();
    protected static Context context = UI.getContext;

    public TrafficSQL() {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
        Log.w(TAG, "constructor");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.CREATE_TrafficInfo_SQL);
        db.execSQL(Constants.CREATE_Plans_SQL);
        Log.w(TAG, "onCreate ---- start");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + Constants.CREATE_TrafficInfo_SQL);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.CREATE_Plans_SQL);
        onCreate(db);
    }

    public void onDelete(SQLiteDatabase db) {
        db.delete(Constants.TABLE_Traffic, null, null);
        db.delete(Constants.TABLE_Plans, null, null);
    }
}