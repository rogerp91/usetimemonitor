package co.usetime.monitor.domain.model;

import android.graphics.drawable.Drawable;

public class TrafficInfo {

    private int id;
    private long traffic;
    private String packageName;
    private String appName;
    private Drawable appicon;
    private String date;
    private Long total = (long) 0;
    private Long GPRS = (long) 0;
    private Long WIFI = (long) 0;

    public String getDate() {
        return date;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getGPRS() {
        return GPRS;
    }

    public void setGPRS(Long gPRS) {
        GPRS = gPRS;
    }

    public Long getWIFI() {
        return WIFI;
    }

    public void setWIFI(Long wIFI) {
        WIFI = wIFI;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Drawable getAppicon() {
        return appicon;
    }

    public void setAppicon(Drawable appicon) {
        this.appicon = appicon;
    }

    public long getTraffic() {
        return traffic;
    }

    public void setTraffic(long traffic) {
        this.traffic = traffic;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
