package co.usetime.monitor.domain.model;

import java.util.List;

/**
 * Created by Roger Patiño on 20/05/2016.
 */
public class Nuevo {

    int su;
    int su2;
    List<PrintTraffic> getSumWifi;

    public int getSu() {
        return su;
    }

    public void setSu(int su) {
        this.su = su;
    }

    public int getSu2() {
        return su2;
    }

    public void setSu2(int su2) {
        this.su2 = su2;
    }

    public List<PrintTraffic> getGetSumWifi() {
        return getSumWifi;
    }

    public void setGetSumWifi(List<PrintTraffic> getSumWifi) {
        this.getSumWifi = getSumWifi;
    }
}