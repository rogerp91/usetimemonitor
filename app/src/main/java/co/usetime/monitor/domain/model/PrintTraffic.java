package co.usetime.monitor.domain.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Roger Patiño on 18/05/2016.
 */
public class PrintTraffic {

    public String appName;
    public Drawable appIcon = null;
    private int mMovilKb;
    private int mWifiKb;


    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public int getmMovilKb() {
        return mMovilKb;
    }

    public void setmMovilKb(int mMovilKb) {
        this.mMovilKb = mMovilKb;
    }

    public int getmWifiKb() {
        return mWifiKb;
    }

    public void setmWifiKb(int mWifiKb) {
        this.mWifiKb = mWifiKb;
    }
}