package co.usetime.monitor.domain.model;

/**
 * Created by Roger Patiño on 19/05/2016.
 */
public class WiMobil {

    private Long wifi;
    private Long movil;

    public WiMobil(Long wifi, Long movil) {
        this.wifi = wifi;
        this.movil = movil;
    }

    public Long getWifi() {
        return wifi;
    }

    public void setWifi(Long wifi) {
        this.wifi = wifi;
    }

    public Long getMovil() {
        return movil;
    }

    public void setMovil(Long movil) {
        this.movil = movil;
    }
}