package co.usetime.monitor.domain.repository.interfaces;

import java.util.List;

import co.usetime.monitor.domain.model.TrafficInfo;

/**
 * Created by Roger Patiño on 25/05/2016.
 */
public interface TrafficSQLRepository {

    void open();

    void close();

    void insertTrafficGPRS(long traffic, String packagename);//inserta en 3g o lo que sea movil

    void insertTrafficWIFI(long traffic, String packagename);//inserta en wifi

    List<TrafficInfo> queryByTime(String time);//buscar por fecha

    List<TrafficInfo> query();//buscar tosos

    List<TrafficInfo> queryWIFI();//buscar todo por wifi

    Long queryWIFINow();//buscar todo por wifi

    Long queryGPRSNow();//buscar todo por wifi

    List<TrafficInfo> queryByTimeWIFI(String time);//buscar por fecha en wifi

    List<TrafficInfo> queryWIFI(String packagename);//buscar por wifi por paquete

    List<TrafficInfo> queryGPRS();//buscar todo por movil

    List<TrafficInfo> queryGPRS(String packagename);//bucscar por movil

    List<TrafficInfo> queryByTimeGPRS(String time);//buscar por fecha por movil

    Long querySumWifi();//buscar por fecha por movil

    Long querySumGPRS();//buscar por fecha por

    List<TrafficInfo> getTotalPackageGPRS();

    List<TrafficInfo> getTotalPackageWIFI();

    int getCount();

    List<TrafficInfo> getTime();

    Long getTotalGPRS();

    Long getTotalWIFI();

    List<TrafficInfo> query2();

    void query3(String dates);

    void clear();
}