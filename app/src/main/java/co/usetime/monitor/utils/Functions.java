package co.usetime.monitor.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import co.usetime.monitor.UI;
import co.usetime.monitor.domain.repository.api.HttpRestClient;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Functions {//funciones de ayuda

    protected static String TAG = Functions.class.getSimpleName();
    private static Context context = UI.getContext;
    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();

    /**
     * @return conexion a internet
     */
    public static boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) UI.getContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * @return tiempo
     */
    public static synchronized String getTime() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static String getName() {
        String name = "";
        boolean error = false;
        try {
            AccountManager accountManager = AccountManager.get(context);
            Account[] accounts = AccountManager.get(context).getAccountsByType(Constants.USETIME);
            for (Account account : accounts) {
                name = accountManager.getUserData(account, Constants.KEY_NAME);
                //System.out.println(TAG + " Accout Usetime: " + name);
            }
        } catch (Exception e) {
            Log.i("Exception", "Exception:" + e);
            error = true;
        }

        if (error) {
            return null;
        } else {
            return name;
        }
    }

    public static String getId() {
        String url = "";
        boolean error = false;
        try {
            AccountManager accountManager = AccountManager.get(context);
            Account[] accounts = AccountManager.get(context).getAccountsByType(Constants.USETIME);
            for (Account account : accounts) {
                url = accountManager.getUserData(account, Constants.KEY_ID);
                //System.out.println(TAG + " Accout Usetime: " + url);
            }
        } catch (Exception e) {
            Log.i("Exception", "Exception:" + e);
            error = true;
        }
        if (error) {
            return null;
        } else {
            return url;
        }
    }

    /**
     * @param assetPath letra
     * @return devuelve la letra
     */
    public static Typeface getTypeface(String assetPath) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(context.getAssets(), "fonts/" + assetPath + ".ttf");
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
                    Log.e("Typefaces", "Error en typeface '" + assetPath + "' Por " + e.getMessage());
                    return null;
                }
            }
            return typefaceCache.get(assetPath);
        }
    }// end

    public static Gson provideGson(boolean type) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        if (type) {
            return gsonBuilder.create();
        } else {//exponse
            gsonBuilder.excludeFieldsWithoutExposeAnnotation();
            return gsonBuilder.create();
        }
    }

    /**
     * @return crea una instancia okhttp
     */
    public static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    public static OkHttpClient provideOkHttpClient2(int connect, int write, int read) {
        return new OkHttpClient.Builder()
                .connectTimeout(connect, TimeUnit.SECONDS)
                .writeTimeout(write, TimeUnit.SECONDS)
                .readTimeout(read, TimeUnit.SECONDS)
                .build();

    }

    /**
     * @param gson
     * @param okHttpClient
     * @return retronar la insta retrofit par ahacer peticiones pero nececita sel gson para parsiar y okhttp para la cabezera
     */
    public static Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson)).baseUrl(HttpRestClient.BASE_URL).client(okHttpClient).build();
    }

    public static HttpRestClient provideHttpRestClient(Retrofit retrofit) {
        return retrofit.create(HttpRestClient.class);
    }

    public static String getVersion() {
        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = UI.getContext.getPackageManager().getPackageInfo(UI.getContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "";
        }
        version = pInfo.versionName;
        return version;
    }

    public static String floatForm(double d) {
        return new DecimalFormat("#.##").format(d);
    }

    public static String bytesToHuman(long size) {
        long Kb = 1 * 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;
        if (size < Kb) return floatForm(size) + " Byte";
        if (size >= Kb && size < Mb) return floatForm((double) size / Kb) + " KB";
        if (size >= Mb && size < Gb) return floatForm((double) size / Mb) + " MB";
        if (size >= Gb && size < Tb) return floatForm((double) size / Gb) + " GB";
        if (size >= Tb && size < Pb) return floatForm((double) size / Tb) + " TB";
        if (size >= Pb && size < Eb) return floatForm((double) size / Pb) + " PB";
        if (size >= Eb) return floatForm((double) size / Eb) + " Eb";
        return "???";
    }
}