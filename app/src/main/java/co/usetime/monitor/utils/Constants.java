package co.usetime.monitor.utils;

public class Constants {

    public static final String USETIME = "co.usetime";

    //session
    public static final String SESSION = "session";

    //token
    public static final String TOKEN = "token";

    //INSTALL
    public static final String INSTALL = "INSTALL";

    //account
    public static String KEY_SUCCESS = "success";
    public static String KEY_ERROR = "error";
    public static String KEY_ERROR_MSG = "error_msg";
    public static final String ADD_ACCOUNT_TYPE = "co.usetime.account";
    public static final String AUTH_TOKEN_TYPE = "AuthTokenType";
    public static final String KEY_USERNAME = "UserName";
    public static final String KEY_USERFULLNAME = "UserFullName";
    public static final String KEY_AVATARURL = "AvatarUrl";
    public static final String KEY_USERTOKEN = "UserToken";

    //process
    public static final String IS_CLASS = "IS_CLASS";
    public static final String IS_CLASS_DATE = "IS_CLASS_DATE";
    public static final String IS_CLASS_TIME = "IS_CLASS_TIME";

    public static final String IS_PACKAGE = "IS_PACKAGE";
    public static final String IS_PACKAGE_NAME = "IS_NAME";
    public static final String IS_PACKAGE_DATE = "IS_DATE";
    public static final String IS_PACKAGE_TIME = "IS_TIME";
    public static final String IS_PACKAGE_DURATION = "IS_DURATION";

    //process
    public static final String SUDO = "su";

    //path folde
    public static final String PATH_BASE = "UseTime/";
    public static final String PATH_SCREENSHOT = "Screenshot/";
    public static final String SCREENCAP = "/system/bin/screencap -p ";
    //public static final String SDCARD = "/sdcard/";
    public static final String PNG = ".png";
    public static final String JPG = ".jpg";
    public static final String ASCII = "ASCII";

    //btn profile
    public static final String PRIVATE = "private";
    public static final String WORK = "work";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";

    public static final String KEY_ID = "KEY_ID";
    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String KEY_ENTERPRISE_ID = "KEY_ENTERPRISE_ID";
    public static final String KEY_STATUS = "KEY_STATUS";
    public static final String KEY_CREATE_AT = "KEY_CREATE_AT";
    public static final String KEY_UPDATED_AT = "KEY_UPDATED_AT";
    public static final String KEY_AVATAR_URL = "KEY_AVATAR_URL";
    public static final String KEY_ROLE_ID = "KEY_ROLE_ID";
    public static final String KEY_ROLE = "KEY_ROLE";

    /**
     * Intent extra key to retrieve the disk cache size as an integer.
     */
    public static final String EXTRA_DISK_CACHE_SIZE = "EXTRA_DISK_CACHE_SIZE";

    /**
     * Intent extra key to retrieve the ram cache size as an integer.
     */
    public static final String EXTRA_RAM_CACHE_SIZE = "EXTRA_RAM_CACHE_SIZE";

    /**
     * Intent extra key to retrieve the id of the cache as a string.
     */
    public static final String EXTRA_ID_CACHE = "EXTRA_ID_CACHE";

    //data base
    public static String DATABASE_NAME = "usetimeDB";
    public static int DATABASE_VERSION = 1;

    public static String CREATE_TrafficInfo_SQL = "CREATE TABLE Traffic("
            + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "WIFI long default(0),"
            + "GPRS long default(0),"
            + "time TIMESTAMP default (datetime('now', 'localtime')),"
            + "packagename varchar(60))";

    public static String CREATE_Plans_SQL = "CREATE TABLE Plans("
            + "company varchar(30),"
            + "planName varchar(100) primary key,"
            + "planTraffic int,"
            + "planPrice int)";

    public static String TABLE_Traffic = "Traffic";

    public static String TABLE_Plans = "Plans";

}// endClass