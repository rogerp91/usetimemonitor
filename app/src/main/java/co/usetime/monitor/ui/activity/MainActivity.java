package co.usetime.monitor.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.greysonparrelli.permiso.Permiso;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import co.usetime.monitor.R;
import co.usetime.monitor.UI;
import co.usetime.monitor.bus.BusProvider;
import co.usetime.monitor.bus.Unsupported;
import co.usetime.monitor.domain.repository.api.HttpRestClient;
import co.usetime.monitor.domain.repository.imp.TrafficSQLRepositoryImp;
import co.usetime.monitor.domain.repository.interfaces.TrafficSQLRepository;
import co.usetime.monitor.services.TrafficService;
import co.usetime.monitor.services.UploadTrafficService;
import co.usetime.monitor.ui.fragment.AppsFragment;
import co.usetime.monitor.ui.fragment.ViewPagerTraffic;
import co.usetime.monitor.utils.Constants;
import co.usetime.monitor.utils.Prefs;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    protected static String TAG = MainActivity.class.getSimpleName();
    public static Bus bus;
    private DrawerLayout drawerLayout;

    AlertDialog alert = null;
    LogOut logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean session = Prefs.getBoolean(Constants.SESSION, false);
        if (!session) {
            finish();
            startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //welcome.setText(Prefs.getString(Constants.KEY_USERNAME, ""));
        if (session) {
            toolbar();

            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

            if (navigationView != null) {
                prepararDrawer(navigationView);
                seleccionarItem(navigationView.getMenu().getItem(0));
            }

            //add profiles
            View headerNav = navigationView.inflateHeaderView(R.layout.cabecera_drawer);//inflar el header del drawer
            ((TextView) headerNav.findViewById(R.id.username)).setText(Prefs.getString(Constants.NAME, ""));
            ((TextView) headerNav.findViewById(R.id.email)).setText(Prefs.getString(Constants.KEY_USERNAME, ""));
            String url2 = HttpRestClient.BASE_URL + HttpRestClient.VERSION + "profile/" + Prefs.getString(Constants.KEY_ID, "") + "/avatar";
            Picasso.with(this).load(url2).into((CircleImageView) headerNav.findViewById(R.id.circle_image));
        }
    }

    private void toolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.drawer_toggle);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });

    }

    private void seleccionarItem(MenuItem itemDrawer) {
        Fragment fragmentoGenerico = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (itemDrawer.getItemId()) {
            case R.id.item_inicio:
                fragmentoGenerico = new ViewPagerTraffic();
                break;
            case R.id.item_apps:
                fragmentoGenerico = new AppsFragment();
                break;
            case R.id.nav_log_out:
                logOut();
                break;

        }
        if (fragmentoGenerico != null) {
            fragmentManager.beginTransaction().replace(R.id.contenedor_principal, fragmentoGenerico).commit();
        }
        // Setear título actual
        setTitle(itemDrawer.getTitle());
    }


    /**
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Prefs.getBoolean(Constants.SESSION, false)) {
            BusProvider.getInstance().register(this);
            TrafficService.actionStart();
            UploadTrafficService.actionStart(getApplicationContext());
        }

        if (Prefs.getBoolean("UNSUPPORTED", false)) {
            showDialog();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }


    @Subscribe
    public void getBusUNSUPPORTED(Unsupported unsupported) {
        if (unsupported != null) {
            if (unsupported.isaBoolean()) {
                showDialog();
            }
        }
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Dialog");
        builder.setMessage("Hello here is the best example of AppCompatAlertDialog from www.takeoffandroid.com. Lets make use of it");
        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    //salir
    private void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Deseas cerrar Clowd Trace?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        logOut = new LogOut();
                        logOut.execute((Void) null);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    //ir a los fragm
    private class LogOut extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            stopService(new Intent(MainActivity.this, TrafficService.class));
            UploadTrafficService.actionDefinitive(getApplicationContext());
            Log.w(TAG, "doInBackground");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

/**
 stopService(new Intent(MainActivity.this, TrafficService.class));
 stopService(new Intent(MainActivity.this, UploadTrafficService.class));
 stopService(new Intent(MainActivity.this, DataUploadService.class));
 stopService(new Intent(MainActivity.this, EventService.class));
 stopService(new Intent(MainActivity.this, InitService.class));
 stopService(new Intent(MainActivity.this, LapseService.class));
 stopService(new Intent(MainActivity.this, LockerService.class));
 stopService(new Intent(MainActivity.this, ProcessService.class));
 stopService(new Intent(MainActivity.this, UploadAppService.class));
 */
            return true;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool) {
                Log.w(TAG, "onPostExecute");
                Prefs.putBoolean(Constants.SESSION, false);
                try {
                    TrafficSQLRepository trafficSQLRepository = new TrafficSQLRepositoryImp();
                    trafficSQLRepository.clear();
                } catch (SQLiteException e) {
                    Log.d(TAG, e.toString());
                }
                alert.dismiss();
                finish();
                startActivity(new Intent(UI.getContext, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

}