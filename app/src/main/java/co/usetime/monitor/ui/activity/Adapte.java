package co.usetime.monitor.ui.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import co.usetime.monitor.R;
import co.usetime.monitor.UI;
import co.usetime.monitor.domain.model.PrintTraffic;

/**
 * Created by Roger Patiño on 18/05/2016.
 */
public class Adapte extends BaseAdapter {

    List<PrintTraffic> printTraffics;

    public Adapte(List<PrintTraffic> printTraffics) {
        this.printTraffics = printTraffics;
    }

    @Override
    public int getCount() {
        return printTraffics.size();
    }

    @Override
    public PrintTraffic getItem(int position) {
        return printTraffics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater)  UI.getContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_install_application, null);
        }
        PrintTraffic printTraffic = printTraffics.get(position);
        TextView tvAppName = (TextView) v.findViewById(R.id.tvAppName);
        TextView tvAppTraffic = (TextView) v.findViewById(R.id.tvAppTraffic);
        TextView traffic = (TextView) v.findViewById(R.id.traffic);


        tvAppName.setText(printTraffic.getAppName());
        tvAppTraffic.setText(Integer.toString(printTraffic.getmWifiKb()) +" Kb");
        traffic.setText(Integer.toString(printTraffic.getmMovilKb()) +" Kb");

        return v;
    }
}