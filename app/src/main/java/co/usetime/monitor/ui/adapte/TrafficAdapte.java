package co.usetime.monitor.ui.adapte;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.usetime.monitor.R;
import co.usetime.monitor.UI;
import co.usetime.monitor.domain.model.TrafficInfo;
import co.usetime.monitor.utils.CircleDisplay;
import co.usetime.monitor.utils.Functions;

/**
 * Created by Roger Patiño on 22/02/2016.
 */
public class TrafficAdapte extends RecyclerView.Adapter<TrafficAdapte.ViewHolder> {
    protected static String TAG = TrafficAdapte.class.getSimpleName();

    private OnItemClickListener listener;
    List<TrafficInfo> trafficInfos;
    private Context context;
    private int type;

    public TrafficAdapte(List<TrafficInfo> alist, int type) {
        this.trafficInfos = alist;
        context = UI.getContext;
        this.type = type;
    }

    //interfaces
    public interface OnItemClickListener {
        void onItemClick(RecyclerView.ViewHolder item, int position);
    }

    //setter and getter
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Campos respectivos de un item
        private TextView mName;
        private TextView mKb;
        private ImageView mImage;
        private CircleDisplay cd;
        private TrafficAdapte father = null;

        public ViewHolder(View view, TrafficAdapte father) {
            super(view);
            this.father = father;
            view.setOnClickListener(this);
            mName = (TextView) view.findViewById(R.id.name);
            mKb = (TextView) view.findViewById(R.id.kb);
            mImage = (ImageView) view.findViewById(R.id.circle_image);
            cd = (CircleDisplay) view.findViewById(R.id.circleDisplay);
        }

        @Override
        public void onClick(View v) {
            final OnItemClickListener listener = father.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_apps, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TrafficInfo info = trafficInfos.get(position);
        holder.mName.setText(info.getAppName());
        if (type == 1) {
            holder.mKb.setText(Functions.bytesToHuman(info.getWIFI()) + " usados hasta ahora");
            holder.mImage.setImageDrawable(info.getAppicon());
        } else {
            holder.mKb.setText(Functions.bytesToHuman(info.getGPRS()) + " usados hasta ahora");
            holder.mImage.setImageDrawable(info.getAppicon());
        }

        holder.cd.setAnimDuration(3000);
        holder.cd.setValueWidthPercent(55f);
        holder.cd.setTextSize(5f);
        holder.cd.setColor(Color.GREEN);
        holder.cd.setDrawText(true);
        holder.cd.setDrawInnerCircle(true);
        holder.cd.setFormatDigits(1);
        holder.cd.setTouchEnabled(true);
        holder.cd.setUnit("%");
        holder.cd.setStepSize(0.5f);
        // cd.setCustomText(...); // sets a custom array of text
        holder.cd.showValue(75f, 100f, true);

    }

    @Override
    public int getItemCount() {
        return (trafficInfos != null ? trafficInfos.size() : 0);
    }

    @Override
    public long getItemId(int position) {
        return trafficInfos.get(position).getId();
    }

}