package co.usetime.monitor.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.usetime.monitor.R;
import co.usetime.monitor.domain.model.TrafficInfo;
import co.usetime.monitor.domain.repository.imp.TrafficSQLRepositoryImp;
import co.usetime.monitor.domain.repository.interfaces.TrafficSQLRepository;
import co.usetime.monitor.ui.adapte.TrafficAdapte;

public class AppsGPRSFragment extends Fragment {


    protected View view;
    private RecyclerView recycler;
    private TextView text;
    private LinearLayoutManager linearManager;
    private TrafficAdapte adaptador;
    private TrafficSQLRepository trafficSQLRepository = null;
    private Task task;
    private Handler handler = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_apps_gpr, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recycler = (RecyclerView) view.findViewById(R.id.recycler_view);
        text = (TextView) view.findViewById(R.id.text);

        recycler.setHasFixedSize(true);
        linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);

        trafficSQLRepository = new TrafficSQLRepositoryImp();

        if (TrafficStats.getTotalRxBytes() == TrafficStats.UNSUPPORTED && TrafficStats.getTotalTxBytes() == TrafficStats.UNSUPPORTED) {
            text.setText("No soportado");
        } else {
            showText(false);
            showRecycler(true);
            handler = new Handler();
            handler.postDelayed(runnable, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //task = new Task();
        //task.execute();
        if (trafficSQLRepository.getCount() == 0) {
            text.setText("Nada para mostrar...");
        } else {
            showText(false);
            showRecycler(true);
            handler = new Handler();
            handler.postDelayed(runnable, 0);
        }
    }

    public class Task extends AsyncTask<Void, Void, List<TrafficInfo>> {

        @Override
        protected List<TrafficInfo> doInBackground(Void... params) {
            trafficSQLRepository.open();
            return trafficSQLRepository.getTotalPackageGPRS();
        }

        @Override
        protected void onPostExecute(List<TrafficInfo> trafficInfos) {
            super.onPostExecute(trafficInfos);
            if (trafficInfos.size() != 0) {
                adaptador = new TrafficAdapte(trafficInfos, 2);
                recycler.setAdapter(adaptador);
            }
        }
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            task = new Task();
            task.execute();
            handler.postDelayed(runnable, 300000);
        }
    };

    private void showText(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        text.setVisibility(show ? View.VISIBLE : View.GONE);
        text.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                text.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void showRecycler(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        recycler.setVisibility(show ? View.VISIBLE : View.GONE);
        recycler.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                recycler.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}