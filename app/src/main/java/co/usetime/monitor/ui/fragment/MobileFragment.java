package co.usetime.monitor.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import az.plainpie.PieView;
import co.usetime.monitor.R;
import co.usetime.monitor.domain.repository.imp.TrafficSQLRepositoryImp;
import co.usetime.monitor.domain.repository.interfaces.TrafficSQLRepository;
import co.usetime.monitor.utils.Functions;

/**
 * Created by Roger Patiño on 25/05/2016.
 */
public class MobileFragment extends Fragment {

    protected View view;
    private TextView total;
    private TextView total_byte;

    private TextView usado;
    private TextView usado_byte;

    private TextView hoy;
    private TextView hoy_byte;
    private PieView total_pie;

    private TrafficSQLRepository trafficSQLRepository = null;
    private Handler handler = null;

    private double gb = 1073741824;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_movil, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        total = (TextView) view.findViewById(R.id.total);
        total_byte = (TextView) view.findViewById(R.id.total_byte);
        usado = (TextView) view.findViewById(R.id.usado);
        usado_byte = (TextView) view.findViewById(R.id.usado_byte);
        hoy = (TextView) view.findViewById(R.id.hoy);
        hoy_byte = (TextView) view.findViewById(R.id.hoy_byte);
        total_pie = (PieView) view.findViewById(R.id.total_pie);
        total_pie.setPercentageBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        total_pie.setMainBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

    }

    @Override
    public void onResume() {
        super.onResume();
        handler = new Handler();
        handler.postDelayed(runnable, 0);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            trafficSQLRepository = new TrafficSQLRepositoryImp();
            trafficSQLRepository.open();
            Long i = trafficSQLRepository.querySumGPRS();

            if (i == 0) {
                usado.setText("0.00");
                usado_byte.setText("byte");
            } else {

                System.out.println("GTO: ___ " + trafficSQLRepository.queryGPRSNow());

                //total de los datos ej el tlf
                String[] shorts = Functions.bytesToHuman(i).split(" ");
                usado.setText(shorts[0]);
                usado_byte.setText(shorts[1]);

                //plan asignado
                double v = gb - i;
                String[] shorts2 = Functions.bytesToHuman(Double.valueOf(v).longValue()).split(" ");
                total.setText(shorts2[0]);
                total_byte.setText(shorts2[1]);

                //pi chart
                double sum = ((i) / gb) * 100;
                if (sum == 0) {
                    total_pie.setInnerText("0.00" + "%");
                } else {
                    total_pie.setInnerText(Functions.floatForm(sum) + "%");
                }

                //los datos de hoy
                String[] shorts3 = Functions.bytesToHuman(trafficSQLRepository.queryGPRSNow()).split(" ");
                hoy.setText(shorts3[0]);
                hoy_byte.setText(shorts3[1]);
            }
            handler.postDelayed(runnable, 60000);
        }
    };
}