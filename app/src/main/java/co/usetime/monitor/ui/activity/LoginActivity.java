package co.usetime.monitor.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import co.usetime.monitor.R;
import co.usetime.monitor.UI;
import co.usetime.monitor.domain.model.Login;
import co.usetime.monitor.domain.model.Profile;
import co.usetime.monitor.domain.model.Token;
import co.usetime.monitor.domain.repository.api.HttpRestClient;
import co.usetime.monitor.utils.Constants;
import co.usetime.monitor.utils.Functions;
import co.usetime.monitor.utils.Prefs;
import co.usetime.monitor.utils.ProgressWheel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    protected static String TAG = LoginActivity.class.getSimpleName();

    private Toolbar toolbar;

    //input
    private EditText inputEmail;
    private EditText inputPassword;
    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPassword;
    private LinearLayout linearLayout_container;

    private ScrollView mForm;
    private ProgressWheel mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        linearLayout_container = (LinearLayout) findViewById(R.id.linearLayout_container);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputEmail.addTextChangedListener(textWatcher);
        inputPassword.addTextChangedListener(textWatcher);

        mForm = (ScrollView) findViewById(R.id.scroll_form);
        mProgress = (ProgressWheel) findViewById(R.id.progress);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cancel = false;
                validateCredentials(inputEmail.getText().toString(), inputPassword.getText().toString());
            }
        });

    }

    public void validateCredentials(String email, String password) {
        //verificar que no hay error
        boolean cancel = false;
        if (TextUtils.isEmpty(password)) {
            cancel = true;
            showMsg(getResources().getString(R.string.not_empty_password), ContextCompat.getColor(getApplicationContext(), R.color.warning));
        } else {
            if (!(password.length() > 3)) {
                cancel = true;
                showMsg(getResources().getString(R.string.min_length_password), ContextCompat.getColor(getApplicationContext(), R.color.warning));
            }
        }

        if (TextUtils.isEmpty(email)) {
            cancel = true;
            showMsg(getResources().getString(R.string.not_empty_user), ContextCompat.getColor(getApplicationContext(), R.color.warning));
        } else {
            if (!(email.length() > 3)) {
                cancel = true;
                showMsg(getResources().getString(R.string.min_length_user), ContextCompat.getColor(getApplicationContext(), R.color.warning));
                ;
            }
        }
        if (!cancel) {
            if (Functions.isOnline()) {
                showView(true);//quitar el progress
                if(Functions.isOnline()){
                    postLogin(email, password);
                }
            } else {
                showView(false);//quitar el progress
                showMsg(getResources().getString(R.string.no_connection2), ContextCompat.getColor(getApplicationContext(), R.color.warning));
            }
        }
    }

    //varificar si el cursor esta borrando las letras
    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            inputLayoutEmail.setErrorEnabled(false);
            inputLayoutPassword.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void showMsg(String msg, int color) {
        Snackbar snackbar = Snackbar.make(linearLayout_container, msg, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(color);
        snackbar.show();
    }


    public void showView(final boolean show) {
        if (mForm != null && mProgress != null) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mForm.setVisibility(show ? View.GONE : View.VISIBLE);
            mForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    private void postLogin(String email, String password) {
        HttpRestClient restClient = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        Call<Token> tokenCall = restClient.performLogin(HttpRestClient.USER_AGENT + Functions.getVersion(), new Login(email, password));
        tokenCall.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() == null || response.body().getToken().equals("")) {
                        } else {
                            Token token = response.body();
                            getProfile(token.getToken());
                        }
                        break;
                    case 204:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 400:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 401:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.validate), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 403:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 404:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 422:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 500:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.e(TAG, t.toString());
                showView(false);//quitar el progress
                showMsg(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
            }
        });
    }

    private void getProfile(final String token) {
        HttpRestClient restClient = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        Call<Profile> profileCall = restClient.performProfile(HttpRestClient.PREFIX + token);
        profileCall.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null || !response.body().equals("")) {
                            Profile profile = response.body();
                            Prefs.putString(Constants.TOKEN, token);
                            Prefs.putString(Constants.NAME, profile.getName());
                            Prefs.putString(Constants.EMAIL, profile.getEmail());

                            //System.out.println(TAG + " - Profile - " + response.body().getEmail());
                            //System.out.println(TAG + " - Profile - " + response.body().getRole().getName());
                            //System.out.println(TAG + " - Profile - " + response.body().getEmail());
                            onSuccess(profile);
                        } else {
                            showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));

                        }
                        break;
                    case 204:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 400:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 401:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.validate), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 403:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 404:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 422:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    case 500:
                        showView(false);//quitar el progress
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        showMsg(getResources().getString(R.string.msg_error_many), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                showView(false);//quitar el progress
                showMsg(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
            }
        });
    }

    /**
     * @param profile
     */
    public void onSuccess(Profile profile) {
        UI app = (UI) getApplicationContext();
        app.createFolders();
        if (profile != null) {
            newAccount(profile);
        }
    }

    /**
     * @param profile
     */
    private void newAccount(Profile profile) {
        Prefs.putString(Constants.KEY_ENTERPRISE_ID, profile.getEnterpriseId());
        Prefs.putString(Constants.KEY_ID, profile.getId());
        Prefs.putString(Constants.KEY_USERNAME, profile.getEmail());
        Prefs.putBoolean(Constants.SESSION, true);
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

}