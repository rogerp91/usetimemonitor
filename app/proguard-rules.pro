# Add project specific ProGuard rules here.
# By profile_default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
find . -name '*.java' -exec gsed -i 's/import retrofit\./import retrofit2./g' \{\} +

find . -name '*.java' -exec gsed -i 's/import com.squareup.okhttp/import okhttp3/g' \{\} +