# UsetimeMonitor

Android - Code - Screenshot

------

# Descripción

UsetimeMonitor es una aplicación desarrollada en Android basada en la nube para el control de uso de Wi-FI y datos móviles, en la cual ayudara hacer un seguimiento de su uso de datos y asegurarse de no se excederse de su cuenta mensual de datos. La información del consumo de dato es almacenada en servidores para realizar cálculos diarios y mensuales desde la web.
Tecnologías Utilizadas: JodaTime, ButterKnife, Retrofit, GSON, Otto, ZurchePlainPie, Firebase

---

# Descripción

![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503365300/34780add-cf77-4158-8de3-8419d0c631cd-large_xbh3of.jpg)